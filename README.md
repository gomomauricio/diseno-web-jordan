# Código del curso DISEÑO WEB MODERNO DESDE CERO A AVANZADO HTML5 Y CSS3

# Instructor
> [Jordan Alexander](https://www.udemy.com/course/curso-diseno-web-moderno-desde-cero/) 
 

# Udemy
* DISEÑO WEB MODERNO HTML5 Y CSS3

## Contiene

* HTML5
	* Crear y Leer documentos HTML5
	* Usar elementos SVG en HTML5
	* Maquetación de formularios con HTML5
	* Entender documentos en HTML5
	* Crear contenido en HTML5
	* Bases de HTML5

* CSS3
	* Crear y leer hojas de estilo CSS3
	* Entender la herencia, cascada y especificidad
	* Entender el modelo de caja
	* Bases de CSS3

* Diseño web adaptable a dispositivos móviles
* Creación de sitios web adaptables a celular
* Flexbox (Cajas flexibles)
* Creación de sitios web con Flexbox
* CSS GRID
* Animaciones en CSS3


 

## Descripción
     Diseño de sitios web modernos, con HTML y CSS,entrando al mundo del frontend.

      

---

## Notas
 
~~~
* Repositorio solo en ** Gitlab  **

 
 ~~~


---
## Código 

 
 

---
#### Version  V.0.1 
* **V.0.1**  _>>>_  Iniciando proyecto [  on 05 DIC, 2022 ]  
 
 